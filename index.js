import fastifyModule from 'fastify'
const fastify = fastifyModule()

// HANDLERS

const helloWorldHandler = ( request, reply ) => {
    return reply.send( { hello: 'world' } )
}

const formHandler = ( req, res ) => {
    return res.send( 'form received' )
}

// ROUTES

fastify.get( '/', helloWorldHandler )

fastify.post( '/form', formHandler )

// SERVER

fastify.listen( 8080, function ( err ) {
    if ( err ) throw err
    console.log( `server listening on ${fastify.server.address().port}` )
} )
